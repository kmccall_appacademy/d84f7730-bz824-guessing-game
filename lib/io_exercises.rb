# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  puts "guess a number\n"
  player_number = gets.chomp.to_i
  turns = 1
  answer = []
  secret_number = rand(1..100)

  while (player_number != secret_number )
    puts "#{player_number}"
    puts "too #{player_number < secret_number ? "low" : "high"}."
    print "=> "
    player_number = gets.chomp.to_i
    raise NoMoreInput if answer.include?(player_number)
    answer << player_number
    turns += 1
  end

  render(player_number, turns)
end

def render(player_number, turns)
  puts "#{player_number}\n"
  puts "#{turns}"
end

if __FILE__ == $PROGRAM_NAME
  puts "Please enter a filename!"
  file1 = gets.chomp
  file2 = File.open("shuffle", 'w')
  File.open(file1).each do |line|
    shuffled = line.chars.shuffle.join
    file2.write(shuffled)
  end
  file2.close
end
